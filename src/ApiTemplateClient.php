<?php

namespace Drupal\apitemplate_io;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response as GuzzleHttpResponse;

/**
 * Provides a client to interact with APITemplate.io.
 */
class ApiTemplateClient {

  /**
   * The http_client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The apitemplate_io.settings config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The logger.channel.apitemplate_io service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs an ApiTemplateClient instance.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory, LoggerChannelInterface $logger) {
    $this->httpClient = $http_client;
    $this->config = $config_factory->get('apitemplate_io.settings');
    $this->logger = $logger;
  }

  /**
   * Allows overridding any key under apitemplate_io.settings config.
   *
   * This is a simple work-around to allow testing new config values, before
   * actually saving them. The override is only active for the current request
   * and is *not* persisted.
   */
  public function tempConfigOverride(array $conf_overrides) {
    // Only allow keys already defined in the config object; prevent overrides
    // to '_core'.
    $valid_keys = array_diff(array_keys($this->config->getRawData()), ['_core']);
    // Filter out invalid keys from the overrides.
    $conf_overrides = array_intersect_key($conf_overrides, array_flip($valid_keys));

    $this->config->setSettingsOverride($conf_overrides);
  }

  /**
   * Send a request to APITemplate.io' REST API.
   *
   * @param string $op
   *   The operation to perform; e.g. 'create-pdf'.
   * @param array $json_vars
   *   (Optional) array with variables to use as the JSON payload.
   * @param array $query_vars
   *   (Optional) array with variables to use as the url query.
   *
   * @return \GuzzleHttp\Psr7\Response
   *   The response.
   */
  public function request($op, array $json_vars = [], array $query_vars = []) {
    $ops_method_map = [
      'create-pdf' => 'POST',
      'merge-pdfs' => 'POST',
      'list-templates' => 'GET',
      'delete-object' => 'GET',
    ];
    $method = $ops_method_map[$op];
    $op_endpoint = $this->config->get('api_endpoint') . '/v2/' . $op;

    try {
      return $this->httpClient->request($method, $op_endpoint, [
        'json' => $json_vars,
        'query' => $query_vars,
        'headers' => [
          'X-API-KEY' => $this->config->get('api_key'),
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (\Exception $e) {
      $this->logger->error('HTTP request failed with message: %msg', [
        '%msg' => $e->getMessage(),
      ]);
      // Use null so we can call ::parseResponse() directly, in order to keep
      // error-handling super-simple.
      return NULL;
    }
  }

  /**
   * Parses a JSON response from the API.
   *
   * @param \GuzzleHttp\Psr7\Response|null $response
   *   The response object.
   * @param bool $raw_body
   *   Whether to return the raw response content (if TRUE) or after passing it
   *   via json_decode().
   *
   * @return array|string|bool
   *   Either the parsed JSON (as an array) or the raw response content; FALSE
   *   if no valid response is passed or the HTTP response status code is not
   *   200-OK.
   */
  public function parseResponse(?GuzzleHttpResponse $response, bool $raw_body = FALSE) {
    if (!$response) {
      return FALSE;
    }

    // Body contents can be read only once from a guzzle response.
    $response_content = $response->getBody()->getContents();

    // If successful, return the decoded response.
    if ($response->getStatusCode() === Response::HTTP_OK) {
      return $raw_body ?
        $response_content :
        (json_decode($response_content, TRUE) ?? FALSE);
    }
    else {
      $this->logger->error("Received response with HTTP status <strong>%status</strong>; response body: \n<pre>%body</pre>", [
        '%status' => $response->getStatusCode(),
        '%body' => $response_content,
      ]);
    }

    return FALSE;
  }

  /**
   * Gets the list of templates.
   *
   * @return array|false
   *   The array of templates; false on failure.
   */
  public function listTemplates() {
    $resp_json = $this->parseResponse($this->request('list-templates', [], ['format' => 'PDF']));
    return $resp_json['templates'] ?? FALSE;
  }

  /**
   * Creates a PDF file.
   *
   * @param array $body_vars
   *   The vars to use in the template.
   * @param string $template_id
   *   (Optional) the id of the template to use. Defaults to the globally
   *   configured default template id.
   * @param bool $return_raw_body
   *   (Optional) whether to return the parsed body as raw (maybe binary) data
   *   or just the PDF url. Defaults to FALSE.
   *
   * @return string|false
   *   Either the PDF url (if $return_raw_body is FALSE) or the raw binary
   *   data; FALSE if the operation failed.
   */
  public function createPdf(array $body_vars, $template_id = NULL, $return_raw_body = FALSE) {
    $template_id = $template_id ?: $this->config->get('default_template_id');

    if (empty($template_id)) {
      return FALSE;
    }

    $response = $this->request('create-pdf', $body_vars, [
      'template_id' => $template_id,
      'export_type' => $return_raw_body ? 'file' : 'json',
    ]);
    $parsed_response = $this->parseResponse($response, $return_raw_body) ?: [];

    return $return_raw_body ?
      $parsed_response :
      ($parsed_response['download_url'] ?? FALSE);
  }

  /**
   * Merges 2 or more PDFs.
   *
   * @param array $urls
   *   The list of urls for the PDFs to merge.
   * @param bool $return_raw_body
   *   (Optional) whether to return the parsed body as raw (maybe binary) data
   *   or just the PDF url. Defaults to FALSE.
   *
   * @return string|false
   *   Either the PDF url (if $return_raw_body is FALSE) or the raw binary
   *   data; FALSE if the operation failed.
   */
  public function mergePdfs(array $urls, $return_raw_body = FALSE) {
    $urls = array_map(function ($url) {
      return str_starts_with($url, 'http') ?
        $url :
        Utility::getDataUriFromBuffer($url);
    }, $urls);
    $response = $this->request('merge-pdfs', [
      'urls' => $urls,
      'export_type' => $return_raw_body ? 'file' : 'json',
    ], []);
    $parsed_response = $this->parseResponse($response, $return_raw_body) ?: [];

    return $return_raw_body ?
      $parsed_response :
      ($parsed_response['primary_url'] ?? FALSE);
  }

  /**
   * Deletes an object from a previous transaction.
   *
   * @param string $transaction_ref
   *   The transaction_ref value.
   *
   * @return array|false
   *   Either the array for a successful JSON response, or FALSE for failure.
   */
  public function deleteObject($transaction_ref) {
    $query_vars = ['transaction_ref' => $transaction_ref];
    return $this->parseResponse($this->request('delete-object', [], $query_vars));
  }

  /**
   * Builds a response to serve a local download for an external file.
   *
   * @param string $url_or_content
   *   Either the raw contents for the download or the url from which to fetch
   *   the download contents.
   * @param string $filename
   *   The filename to use for the download.
   * @param bool $resolve_url
   *   Whether to allow fetching the content using $url_or_content as url.
   *
   * @return \Symfony\Component\HttpFoundation\Response|false
   *   The response for a download; FALSE if fetching the external url fails.
   */
  public function serveFile($url_or_content, $filename, $resolve_url = TRUE) {
    $content = FALSE;

    if ($resolve_url) {
      $response = $this->httpClient->request('GET', $url_or_content, ['http_errors' => FALSE]);
      if ($response->getStatusCode() === Response::HTTP_OK) {
        $content = $response->getBody()->getContents();
        $content_length = $response->getHeaders()['Content-Length'];
        $content_type = $response->getHeaders()['Content-Type'];
      }
    }
    else {
      $content = $url_or_content;
      $content_length = strlen($content);
      $content_type = Utility::getMimeTypeFromBuffer($content);
    }

    if ($content) {
      $output_response = new Response();
      $output_response->headers->set('Cache-Control', 'private');
      $output_response->headers->set('Content-Type', $content_type);
      $output_response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '";');
      $output_response->headers->set('Content-Length', $content_length);
      $output_response->setContent($content);

      return $output_response;
    }

    return FALSE;
  }

}
