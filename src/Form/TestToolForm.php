<?php

namespace Drupal\apitemplate_io\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Test tool for the API client.
 */
class TestToolForm extends FormBase {

  /**
   * The apitemplate_io.client service.
   *
   * @var \Drupal\apitemplate_io\ApiTemplateClient
   */
  protected $client;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->client = $container->get('apitemplate_io.client');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'apitemplate_io_test_tool';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    $templates = $this->client->listTemplates();

    if (!$templates) {
      if ($templates === FALSE) {
        $this->messenger()->addError($this->t('Failed to get the list of templates from APITemplate.io; please, verify the credentials in the Settings page.'));
      }
      elseif (empty($templates)) {
        $this->messenger()->addError($this->t('No templates found; please add at least 1 template in APITemplate.io dashboard.'));
      }

      return $form;
    }

    $form['template'] = [
      '#type' => 'select',
      '#title' => $this->t('Template'),
      '#options' => array_column($templates, 'name', 'template_id'),
      '#description' => $this->t('Template to use for rendering.'),
      '#required' => TRUE,
    ];

    $form['return_raw_body'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Get the PDF as binary data'),
      '#description' => $this->t('Uses export_type=file to generate the PDF, preventing a copy to be stored on APITemplate.io.'),
      '#default_value' => FALSE,
    ];

    $form['variables'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Variables (JSON)'),
      '#default_value' => '{}',
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Download PDF'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $vars = json_decode($form_state->getValue('variables', ''), TRUE);

    if ($vars === NULL) {
      $form_state->setErrorByName('variables', $this->t('The provided JSON is not valid.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $return_raw_body = $form_state->getValue('return_raw_body', FALSE);
    $template_id = $form_state->getValue('template');
    $vars = json_decode($form_state->getValue('variables', ''), TRUE);
    if ($pdf_result = $this->client->createPdf($vars, $template_id, $return_raw_body)) {
      $form_state->setResponse($this->client->serveFile($pdf_result, 'drupal-apitemplate_io-test.pdf', !$return_raw_body));
    }
    else {
      $this->messenger()->addError($this->t('Failed to generate the PDF. Check the log for details.'));
    }

    $form_state->setRebuild(TRUE);
  }

}
