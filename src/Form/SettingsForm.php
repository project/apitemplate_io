<?php

namespace Drupal\apitemplate_io\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure APITemplate settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The apitemplate_io.client service.
   *
   * @var \Drupal\apitemplate_io\ApiTemplateClient
   */
  protected $client;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->client = $container->get('apitemplate_io.client');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'apitemplate_io_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['apitemplate_io.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('apitemplate_io.settings');

    $form['api_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Endpoint'),
      '#default_value' => $config->get('api_endpoint'),
      '#description' => $this->t('For available endpoints, check the <a href=":api_url" target="_blank">API v2 docs</a>.', [
        ':api_url' => 'https://apitemplate.io/apiv2',
      ]),
    ];
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('api_key'),
    ];
    $form['default_template_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Template ID'),
      '#default_value' => $config->get('default_template_id'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->client->tempConfigOverride([
      'api_endpoint' => $form_state->getValue('api_endpoint', ''),
      'api_key' => $form_state->getValue('api_key', ''),
    ]);
    $templates = $this->client->listTemplates();
    if ($templates === FALSE) {
      $form_state->setErrorByName('api_endpoint', $this->t('Failed to communicate with APITemplate.io at the specified endpoint, using the provided key (called /list-templates).'));
      $form_state->setErrorByName('api_key', $this->t('Failed to communicate with APITemplate.io at the specified endpoint, using the provided key (called /list-templates).'));
    }
    elseif ($templates === []) {
      $this->messenger()->addWarning($this->t('No PDF templates found. Make sure to create at least one template in APITemplate.io.'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('apitemplate_io.settings')
      ->set('api_endpoint', rtrim($form_state->getValue('api_endpoint', ''), '/'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('default_template_id', $form_state->getValue('default_template_id'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
