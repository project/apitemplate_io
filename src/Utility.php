<?php

namespace Drupal\apitemplate_io;

/**
 * Various utilities for apitemplate_io module.
 */
class Utility {

  /**
   * Gets the mime type, given a binary buffer.
   *
   * @param string $buffer
   *   The buffer for which to detect the mime type; binary data is supported.
   *
   * @return string
   *   The detected mime type.
   */
  public static function getMimeTypeFromBuffer($buffer) {
    return (new \finfo(FILEINFO_MIME_TYPE))->buffer($buffer);
  }

  /**
   * Generates a data:... URI, given a binary buffer.
   *
   * @param string $buffer
   *   String with binary data.
   *
   * @return string
   *   The generated data:... URI.
   */
  public static function getDataUriFromBuffer($buffer) {
    $mime_type = static::getMimeTypeFromBuffer($buffer);
    $encoded_content = base64_encode($buffer);
    return "data:$mime_type;base64,$encoded_content";
  }

}
